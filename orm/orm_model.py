from sqlalchemy import Column, String, Integer, Float, Table, ForeignKey, BigInteger
from sqlalchemy.orm import relationship
from base import Base


chemical_company_relation = Table(
    'chemical_company', Base.metadata,
    Column('chemid', BigInteger, ForeignKey('chemicals.chemid')),
    Column('companyid', BigInteger, ForeignKey('company.companyid'))
)

chemical_reaction_relation = Table(
    'chemical_reaction', Base.metadata,
    Column('chemid', BigInteger, ForeignKey('chemicals.chemid')),
    Column('reactionid', BigInteger, ForeignKey('reaction.reactionid'))
)

class Chemicals(Base):
    __tablename__ = 'chemicals'

    chemid = Column(BigInteger, primary_key=True)
    name = Column(String)
    formula = Column(String)
    applications = Column(String)
    infolink = Column(String)
    founderid = Column(Integer, ForeignKey('founder.founderid'))
    founder = relationship("Founder", backref = 'chemicals')

    def __init__(self, chemid, name, formula, applications, infolink, founderid):
        self.chemid = chemid
        self.name = name
        self.formula = formula
        self.applications = applications
        self.infolink = infolink
        self.founderid = founderid


class Company(Base):
    __tablename__ = 'company'

    companyid = Column(Integer, primary_key=True)
    company_name = Column(String)
    address = Column(String)
    website = Column(String)
    phone = Column(BigInteger)
    chemicals = relationship('Chemicals', secondary = chemical_company_relation)


    def __init__(self, companyid, company_name, address, website, phone):
        self.companyid = companyid
        self.company_name = company_name
        self.address = address
        self.website = website
        self.phone = phone



class Reaction(Base):
    __tablename__ = 'reaction'

    reactionid = Column(Integer, primary_key=True)
    reaction_name = Column(String)
    input = Column(String)
    output = Column(String)
    temperature = Column(Float)
    catalyst = Column(String)
    chemicals = relationship('Chemicals', secondary = chemical_reaction_relation)

    def __init__(self, reactionid, reaction_name, input, output, temperature, catalyst):
        self.reactionid = reactionid
        self.reaction_name = reaction_name
        self.input = input
        self.output = output
        self.temperature = temperature
        self.catalyst = catalyst



class Founder(Base):
    __tablename__ = 'founder'

    founderid = Column(Integer, primary_key=True)
    name = Column(String)
    surname = Column(String)
    organization = Column(String)


    def __init__(self, founderid, name, surname, organization):
        self.founderid = founderid
        self.name = name
        self.surname = surname
        self.organization = organization
