from base import Session, engine, Base
from orm_model import Chemicals, Company, Reaction, Founder
from sqlalchemy import select, update


# generate database schema
Base.metadata.create_all(engine)

# create a new session
session = Session()

# Select Statement
conn = engine.connect()
s = select([Chemicals])
result = conn.execute(s)
for i in result:
    print(i)
session.commit()
session.close()


# Update Statement

data = update(Chemicals).where(Chemicals.chemid == 5).\
                            values(name = 'abc')
newdata = conn.execute(data)
session.commit()
session.close()


'''
# Insert Statement
molecule = Chemicals(chemid = 5, name= "xyz", formula = "345", applications = "industry", infolink = "www.xyz.com", founderid = 2)

session.add(molecule)
session.commit()
session.close()

'''

