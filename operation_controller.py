#!/usr/bin/python

import psycopg2
from config import config


def select_chemicals():
    """ query data from the chemicals table """
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT * FROM chemicals ORDER BY chemid")
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()

        while row is not None:
            print(row)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


"""
You can run the select function by activating this statement. Also, you can change
tables and columns that you want to print. An example;
select_chemicals()

"""

def insert_chemicals(name,formula,applications,infolink,founderid,chemid):
    """ insert a new row into the vendors table """
    sql = """INSERT INTO chemicals(name,formula,applications,infolink,founderid,chemid)
             VALUES(%s,%s,%s,%s,%s,%s) RETURNING chemid;"""
    conn = None
    vendor_id = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the INSERT statement
        cur.execute(sql, (name,formula,applications,infolink,founderid,chemid))
        # get the generated id back
        chemid = cur.fetchone()[0]
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    return chemid

"""
if you want to insert row into columns, change the followind values and activate them.
An example;
            name = 'xyz'
            formula = 'CH3OH'
            applications = 'fuel industry, water treatment'
            infolink = 'https://www.wikiwand.com/en/Methanol'
            founderid = 1
            chemid = 4

            insert_chemicals(name,formula,applications,infolink,founderid,chemid)

"""


def update_chemicals(name,formula,applications,infolink,founderid,chemid):
    """ update chemical table based on the chem id """
    sql = """ UPDATE chemicals
                SET name = %s,
                    formula = %s,
                    applications = %s,
                    infolink = %s,
                    founderid = %s
                WHERE chemid = %s """
    conn = None
    updated_rows = 0
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()
        # execute the UPDATE  statement
        cur.execute(sql, (name,formula,applications,infolink,founderid,chemid))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return updated_rows

"""
update information: in order to update a row in database, you can change parameters
for sql query in "update_chemicals" function. Then please give the values that you
want to change in columns or tables. An example;

                name = 'Methanol'
                formula = 'CH3OH'
                applications = 'fuel industry, water treatment'
                infolink = 'https://www.wikiwand.com/en/Methanol'
                founderid = 1
                chemid = 1

                update_chemicals(name,formula,applications,infolink,founderid,chemid)

"""